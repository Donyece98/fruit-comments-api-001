package com.fruitcomments.api;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.repositories.IFruitRepository;

@Component
public class ApiApplicationSeeder {
	
	@Autowired
	protected IFruitRepository fruitRepo;
	
	@EventListener
	public void seeder(ContextRefreshedEvent event) {
		seedFruitsTable();
	}
	
	public void seedFruitsTable() {
		Fruit[] defaultFruits = new Fruit[] {
				new Fruit(1,
						"Banana",
						"A banana is an edible fruit – botanically a berry – produced by several kinds of large herbaceous flowering plants in the genus Musa. In some countries, bananas used for cooking may be called plantains, distinguishing them from dessert banana",
						"http://www.pngall.com/wp-content/uploads/2016/04/Banana-PNG.png")
				,
				new Fruit(2,
						"Apple",
						"An apple is a sweet, edible fruit produced by an apple tree. Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus.",
						"https://images.freshop.com/00852201002228/ad2f58915e3267700906f1025ef8917f_medium.png")
		};
		System.out.println("-------------- Seeding Database -----------------");
		for(Fruit defaultFruit : defaultFruits) {
			Example<Fruit> fruitExample = Example.of(defaultFruit);
			Optional<Fruit> fruitResult = ((QueryByExampleExecutor<Fruit>) fruitRepo).findOne(fruitExample);
			if(!fruitResult.isPresent()) {
				((CrudRepository<Fruit, Integer>) fruitRepo).save(defaultFruit);
				System.out.println("Saved Fruit - "+defaultFruit);
			}else {
				System.out.println("Found Fruit - "+defaultFruit);
			}
		}
		System.out.println("-------------- Seeding Complete -----------------");
		
	}

}
