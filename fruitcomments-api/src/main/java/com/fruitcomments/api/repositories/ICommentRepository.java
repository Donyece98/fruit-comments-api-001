package com.fruitcomments.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fruitcomments.api.entities.Comment;

public interface ICommentRepository
  extends JpaRepository<Comment,Integer>
{

}
