package com.fruitcomments.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.repositories.IFruitRepository;

@Component
public class FruitService {

	@Autowired
	protected IFruitRepository fruitRepo; 
	
	public List<Fruit> getAll(){
		return fruitRepo.findAll();
	}
}
