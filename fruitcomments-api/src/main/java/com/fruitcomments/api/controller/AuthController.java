package com.fruitcomments.api.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fruitomments.api.viewmodels.LoginViewModel;

@RestController
@RequestMapping("api/auth")
public class AuthController {
	
	@GetMapping("/hello")
	public String hello() {
		return "Hello from the Other Side"; 
	}
	
	@GetMapping("/sayHello")
	public String sayHello(
		@RequestParam("firstname")String firstname, 
		@RequestParam("lastname")java.util.Optional<String> lastname
		)
	{
		return String.format("Hello %s %s ",firstname, lastname);
	}
	
	@GetMapping("/user/{username}")
	public LoginViewModel getUser(@PathVariable("username")String username) {
		LoginViewModel data = new LoginViewModel(username, "never send password back to user"); 
		return data; 
		
	}
	
	@PostMapping("/login")
	public String login(@RequestBody LoginViewModel credentials) {
		return String.format("Fake-Token-%s-%s", credentials.getUsername(), credentials.getPassword());
	}
}


