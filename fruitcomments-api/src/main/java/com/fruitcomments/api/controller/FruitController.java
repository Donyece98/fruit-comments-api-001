package com.fruitcomments.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.services.FruitService;

@RestController
@RequestMapping("/api/fruits")
public class FruitController {

	@Autowired
	protected FruitService fruitService; 
	
	@GetMapping("")
	public List<Fruit>getAll(){
		return fruitService.getAll(); 
	}
}
